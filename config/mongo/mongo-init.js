db.createUser(
  {
    user: 'admin',
    pwd: 'admin',
    roles: [{ role: 'readWrite', db: 'query' }],
  },
);

db.createCollection('employees');