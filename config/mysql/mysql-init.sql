DROP DATABASE IF EXISTS `command`;
CREATE DATABASE `command`;
USE `command`;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,	
  `employeeId` varchar(32) NOT NULL UNIQUE,
  `userName` varchar(32) NOT NULL UNIQUE,
  `passWord` varchar(32) NOT NULL,
  `nickName` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

GRANT ALL PRIVILEGES ON command.* TO 'admin'@'%';