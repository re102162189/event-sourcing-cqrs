docker-compose down
docker rmi $(docker images -a|grep "<none>"|awk '$1=="<none>" {print $3}')
docker volume prune
docker rmi proxy springio-query springio-command