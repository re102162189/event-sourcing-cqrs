### Hardware/Software Prerequirements
* docker (v19.03.8) [install](https://docs.docker.com/engine/install/)  
* Kafka GUI [Conduktor](https://www.conduktor.io/)  
* MongoDB GUI [Compass](https://www.mongodb.com/try/download/compass)  
* MySQL GUI [Sequel Pro](https://www.sequelpro.com/)  
* Test under macOS High Sierra 10.13.4, CPU: 2.5GHz Interl Core i7 / RAM: 16GB 2133MHz LPDDR3  

### Knowledge base
* [CQRS](https://martinfowler.com/bliki/CQRS.html)
* [Event Souring](https://martinfowler.com/eaaDev/EventSourcing.html)
* [Idempotence](https://en.wikipedia.org/wiki/Idempotence)
* [Kafka](https://kafka-tutorials.confluent.io/kafka-console-consumer-producer-basics/kafka.html)

### Deploy Environment with spring boot, Zookeeper, Kafka, MongoDB, MySQL
```
sh init.sh
```

### Test UI entry URL 
```
http://localhost

(kite / 123456)
```

### Remove Environment 
```
yes | sh destroy.sh
```