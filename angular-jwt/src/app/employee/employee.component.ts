import { Component, OnInit } from "@angular/core";
import { EmployeeService, Employee } from "../service/employee.service";

@Component({
  selector: "app-employee",
  templateUrl: "./employee.component.html",
  styleUrls: ["./employee.component.css"]
})
export class EmployeeComponent implements OnInit {
  employees: Employee[];
  displayedColumns: string[] = ["userName", "employeeId", "delete"];

  constructor(private employeeService: EmployeeService) {}

  ngOnInit() {
    this.employeeService
          .getEmployees()
          .subscribe(response => this.handleSuccessfulResponse(response));
  }

  handleSuccessfulResponse(response: Employee[]) {
    this.employees = response;
  }

  deleteEmployee(employee: Employee): void {
    this.employeeService.deleteEmployee(employee).subscribe(
      data => {
        this.employees = this.employees.filter(u => u !== employee);
      },
      error => {
        console.error(error.message);
      }
    );
  }
}
