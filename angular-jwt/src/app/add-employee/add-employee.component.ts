import { Component, OnInit } from "@angular/core";
import { EmployeeService, Employee } from "../service/employee.service";
import { Router } from '@angular/router';

@Component({
  selector: "app-add-employee",
  templateUrl: "./add-employee.component.html",
  styleUrls: ["./add-employee.component.css"]
})
export class AddEmployeeComponent implements OnInit {
  employee: Employee = new Employee("", "", "", "");

  constructor(private employeeService: EmployeeService,
    private router: Router) {}

  ngOnInit() {}

  createEmployee(): void {
    this.employeeService.createEmployee(this.employee).subscribe(
      data => {
        this.router.navigate([''])
      },
      error => {
        console.error(error.message);
      }
    );
  }
}
