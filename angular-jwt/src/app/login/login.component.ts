import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string = '';
  password: string = '';

  @Input() error: string | null;

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() { }

  checkLogin() {
    this.authService.authenticate(this.username, this.password).subscribe(
      data => {
        this.router.navigate([''])
      },
      error => {
        this.error = error.message;
      }
    )
  }
}