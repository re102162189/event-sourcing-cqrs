import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";


@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private httpClient: HttpClient) {}

  authenticate(username: string, password: string) {
    return this.httpClient
      .post<any>("/query/authenticate", { username: username, password: password })
      .pipe(
        map(userData => {
          sessionStorage.setItem("username", username);
          sessionStorage.setItem("token", userData.token);
          return userData;
        })
      );
  }

  isUserLoggedIn() : boolean {
    let user = sessionStorage.getItem("username");
    return !(user === null);
  }

  logOut(): void {
    sessionStorage.removeItem("username");
  }
}
