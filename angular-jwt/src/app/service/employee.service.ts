import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

export class Employee {
  constructor(
    public employeeId: string,
    public passWord: string,
    public userName: string,
    public nickName: string
  ) { }
}

@Injectable({
  providedIn: "root"
})
export class EmployeeService {
  constructor(private httpClient: HttpClient) { }

  public getEmployees(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>("/query/employee/all");
  }

  public deleteEmployee(employee: Employee) {
    return this.httpClient.delete<Employee>("/command/employee/" + employee.employeeId);
  }

  public createEmployee(employee: Employee) {
    return this.httpClient.post<Employee>("/command/employee", employee);
  }
}
