cd angular-jwt
npm install
ng build --prod

cd ..

cd springio-command
mvn clean package

cd ..

cd springio-query
mvn clean package

cd ..

docker-compose build
docker-compose up -d
