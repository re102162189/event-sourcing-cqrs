package com.cqrs.service;

import java.util.Objects;
import org.springframework.stereotype.Service;
import com.cqrs.model.Employee;

@Service
public class JwtService {
  
  private static final String USERNAME = "kite";

  private static final String PASSWORD = "123456";
  
  public Employee findByName(String username) {
    
    Objects.requireNonNull(username);
    
    Employee user = null;
    
    if(USERNAME.equals(username)) {
      user = new Employee();
      user.setUserName(USERNAME);
      user.setPassWord(PASSWORD);
    }
    
    return user;
  }
}
