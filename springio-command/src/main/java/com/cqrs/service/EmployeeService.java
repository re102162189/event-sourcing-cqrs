package com.cqrs.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.cqrs.command.EmployeeCommandDao;
import com.cqrs.handler.EmployeeEvent;
import com.cqrs.handler.EmployeeEvent.Command;
import com.cqrs.model.Employee;

@Service
public class EmployeeService {
  private static final Logger logger = LoggerFactory.getLogger(EmployeeService.class);

  private static final String TOPIC = "employee-event";

  @Autowired
  private EmployeeCommandDao userCommandDao;

  @Autowired
  private KafkaTemplate<String, EmployeeEvent> kafkaTemplate;

  @Transactional(propagation = Propagation.REQUIRED,
      transactionManager = "chainedTransactionManager", rollbackFor = Exception.class)
  public void save(Employee employee) {
    userCommandDao.insert(employee);

    EmployeeEvent userEvent = new EmployeeEvent();
    userEvent.setEmployee(employee);
    userEvent.setCommand(Command.INSERT);
    kafkaTemplate.send(TOPIC, userEvent);

    logger.info("sent INSERT event: {}", userEvent);
  }

  @Transactional(propagation = Propagation.REQUIRED,
      transactionManager = "chainedTransactionManager", rollbackFor = Exception.class)
  public void update(Employee employee) {
    userCommandDao.update(employee);

    EmployeeEvent userEvent = new EmployeeEvent();
    userEvent.setEmployee(employee);
    userEvent.setCommand(Command.UPDATE);
    kafkaTemplate.send(TOPIC, userEvent);
  }

  @Transactional(propagation = Propagation.REQUIRED,
      transactionManager = "chainedTransactionManager", rollbackFor = Exception.class)
  public void delete(String employeeId) {
    userCommandDao.delete(employeeId);

    Employee employee = new Employee();
    employee.setEmployeeId(employeeId);

    EmployeeEvent userEvent = new EmployeeEvent();
    userEvent.setEmployee(employee);
    userEvent.setCommand(Command.DELETE);
    kafkaTemplate.send(TOPIC, userEvent);
  }
}
