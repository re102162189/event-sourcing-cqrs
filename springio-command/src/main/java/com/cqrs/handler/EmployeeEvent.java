package com.cqrs.handler;

import com.cqrs.model.Employee;

public class EmployeeEvent {
  
  public enum Command {
    INSERT, UPDATE, DELETE
  }
  
  private Employee employee;
  
  private Command command;

  public Employee getEmployee() {
    return employee;
  }

  public void setEmployee(Employee employee) {
    this.employee = employee;
  }

  public Command getCommand() {
    return command;
  }

  public void setCommand(Command command) {
    this.command = command;
  }

  @Override
  public String toString() {
    return "UserEvent [employee=" + employee + ", command=" + command + "]";
  }
}
