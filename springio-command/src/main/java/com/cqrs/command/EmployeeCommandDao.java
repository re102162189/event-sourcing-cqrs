package com.cqrs.command;

import com.cqrs.model.Employee;

public interface EmployeeCommandDao {

  void insert(Employee emplyee);

  void update(Employee emplyee);

  void delete(String emplyeeId);

}