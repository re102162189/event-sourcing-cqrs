package com.cqrs.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.transaction.ChainedTransactionManager;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.kafka.transaction.KafkaTransactionManager;
import com.cqrs.handler.EmployeeEvent;

@Configuration
public class MybatisConfig {
  
  @Bean
  @Autowired
  public DataSourceTransactionManager transactionManager(DataSource dataSource) {
    DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
    transactionManager.setDataSource(dataSource);
    return transactionManager;
  }

  @Bean(name = "chainedTransactionManager")
  public ChainedTransactionManager chainedTransactionManager(DataSourceTransactionManager dataSourceTransactionManager,
                                                             KafkaTransactionManager<String, EmployeeEvent> kafkaTransactionManager) {
      return new ChainedTransactionManager(dataSourceTransactionManager, kafkaTransactionManager);
  }
}
