package com.cqrs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cqrs.model.Employee;
import com.cqrs.service.EmployeeService;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

  @Autowired
  private EmployeeService userService;

  @PostMapping
  public void save(@RequestBody Employee emplyee) {
    userService.save(emplyee);
  }

  @PutMapping
  public void update(@RequestBody Employee emplyee) {
    userService.update(emplyee);
  }

  @DeleteMapping(value = "/{emplyeeId}")
  public void delete(@PathVariable("emplyeeId") String emplyeeId) {
    userService.delete(emplyeeId);
  }
}
