package com.cqrs.service;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import com.cqrs.model.Employee;

@Component("userDetailServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private JwtService jwtService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Override
  public UserDetails loadUserByUsername(String username) {

    Employee user = jwtService.findByName(username);

    if (user == null) {
      throw new UsernameNotFoundException("User not found with username: " + username);
    }

    return new User(user.getUserName(), passwordEncoder.encode(user.getPassWord()),
        new ArrayList<>());

  }

  // private List<GrantedAuthority> getGrantedAuthority(List<Role> roles) {
  // List<GrantedAuthority> authorities = new ArrayList<>(roles.size());
  //
  // for (Role role : roles) {
  // authorities.add(new SimpleGrantedAuthority(role.getName()));
  // }
  // return authorities;
  // }

}
