package com.cqrs.service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cqrs.model.Employee;
import com.cqrs.query.EmployeeQueryDao;

@Service
public class EmployeeService {
  private static final Logger logger = LoggerFactory.getLogger(EmployeeService.class);

  @Autowired
  private EmployeeQueryDao employeeQueryDao;
  
  public List<Employee> getEmployees() {
    List<Employee> employees =  employeeQueryDao.findAll();
    
    logger.info("return users to api rest: {}", employees);
    return employees;
  }

  public Employee getEmployee(String employeeId) {
    logger.info("recrived id from api rest: {}", employeeId);

    Employee employee = employeeQueryDao.findByEmployeeId(employeeId);

    logger.info("return user to api rest: {}", employee);
    return employee;
  }
}
