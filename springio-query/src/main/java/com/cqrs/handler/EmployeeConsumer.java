package com.cqrs.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import com.cqrs.model.Employee;
import com.cqrs.query.EmployeeQueryDao;

@Service
public class EmployeeConsumer {

  private static final Logger logger = LoggerFactory.getLogger(EmployeeConsumer.class);

  private static final String TOPIC = "employee-event";

  private static final String GROUP_ID = "spring-cqrs";
  
  private static final String CONTAINER_FACTORY = "kafkaListenerContainerFactory";

  @Autowired
  private EmployeeQueryDao employeeQueryDao;

  @KafkaListener(topics = TOPIC, groupId= GROUP_ID, containerFactory = CONTAINER_FACTORY)
  public void listen(EmployeeEvent event) {
    Employee employee = event.getEmployee();

    if (employee == null) {
      logger.warn("empty employee in event: {}", event);
      return;
    }

    switch (event.getCommand()) {
      case INSERT:
        logger.info("INSERT event in group: {}", event);
        employeeQueryDao.insert(employee);
        break;
      case UPDATE:
        logger.info("UPDATE event in group: {}", event);
        Employee updateUser = employeeQueryDao.findByEmployeeId(employee.getEmployeeId());
        updateUser.setUserName(employee.getUserName());
        updateUser.setPassWord(employee.getPassWord());
        updateUser.setNickName(employee.getNickName());

        employeeQueryDao.save(updateUser);
        break;
      case DELETE:
        logger.info("DELETE event in group: {}", event);
        Employee delUser = employeeQueryDao.findByEmployeeId(employee.getEmployeeId());
        employeeQueryDao.delete(delUser);
        break;
      default:
        logger.warn("unknown command in event: {}", event);
        break;
    }
  }
}
