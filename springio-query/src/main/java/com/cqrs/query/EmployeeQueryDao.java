package com.cqrs.query;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.cqrs.model.Employee;

public interface EmployeeQueryDao extends MongoRepository<Employee, String> {
  Employee findByEmployeeId(final String employeeId);
}
