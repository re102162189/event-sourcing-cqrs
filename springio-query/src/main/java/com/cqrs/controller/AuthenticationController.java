package com.cqrs.controller;

import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.cqrs.model.JwtRequest;
import com.cqrs.model.JwtResponse;
import com.cqrs.util.JwtTokenUtil;

@RestController
public class AuthenticationController {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  @Qualifier("userDetailServiceImpl")
  private UserDetailsService userDetailsService;

  @PostMapping(value = "/authenticate")
  public ResponseEntity<JwtResponse> authenticate(@RequestBody JwtRequest jwtRequest)
      throws Exception {

    generateAuthentication(jwtRequest.getUsername(), jwtRequest.getPassword());
    
    final UserDetails userDetails = userDetailsService.loadUserByUsername(jwtRequest.getUsername());
    final String token = jwtTokenUtil.generateToken(userDetails);
    
    JwtResponse jwtResponse = new JwtResponse();
    jwtResponse.setToken(JwtTokenUtil.TOKEN_PREFIX + token);
    return ResponseEntity.ok(jwtResponse);
  }

  private void generateAuthentication(String username, String password) throws Exception {
    Objects.requireNonNull(username);
    Objects.requireNonNull(password);
    try {
      authenticationManager
          .authenticate(new UsernamePasswordAuthenticationToken(username, password));
    } catch (DisabledException e) {
      throw new Exception("USER_DISABLED", e);
    } catch (BadCredentialsException e) {
      throw new Exception("INVALID_CREDENTIALS", e);
    }
  }
}
